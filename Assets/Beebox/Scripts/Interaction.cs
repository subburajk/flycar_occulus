﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class Interaction : MonoBehaviour,IPointerClickHandler
{
    public UnityEvent OnClickHandler;
    public void OnPointerClick(PointerEventData eventData)
    {
        OnClickHandler.Invoke();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }
}
