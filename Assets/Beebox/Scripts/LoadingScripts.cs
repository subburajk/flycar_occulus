﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingScripts : MonoBehaviour
{
    float curTimer = 0.5f;

    void Update () {
        curTimer -= Time.deltaTime;
        if(curTimer < 0)
        {
            SceneManager.LoadScene(GlobalData.next_scene_index);
        }

    }
}
