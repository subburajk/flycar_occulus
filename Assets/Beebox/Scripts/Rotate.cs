﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{
    public float f_difX;
    public float speed;
    public bool isSpin = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isSpin) {
            transform.Rotate(Vector3.up, f_difX * speed);
        }
    }

    public void OnToggleSpin()
    {
        isSpin =!isSpin;
    }
}
