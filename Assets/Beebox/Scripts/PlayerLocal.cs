﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerLocal : MonoBehaviour
{
    public static PlayerLocal instance;
    private Vector2 touchPadPos;

    [SerializeField]
    private bool isRotateAround;
    Transform centerObject;
    [SerializeField]
    private float rotatioSpeed = 5;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(this.gameObject);
        }
        DontDestroyOnLoad(this);
        
    }

    public Camera centerEyeCamera;

    public void UpdatePosition(Transform _transform)
    {
        transform.position = _transform.position;
        transform.rotation = _transform.rotation;
        if (DebugLogger.instance!=null) {
            DebugLogger.instance.UpdateDebugCamera(centerEyeCamera, this.transform);
        }
    }

    private void Update()
    {
        if (isRotateAround && centerObject != null)
        {
            touchPadPos = OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad);
            this.transform.RotateAround(centerObject.position, new Vector3(0, -1.0f, 0), touchPadPos.x * Time.deltaTime * rotatioSpeed);
        }
    }

    public void SetRotateAroudObject(Transform _centerObj)
    {
        centerObject = _centerObj;
    }

    public void SetPlayerRotation(bool value,GameObject obj)
    {
        isRotateAround = value;
        centerObject = obj.transform;
    }
}