﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorsMenu : MonoBehaviour
{
    public GameObject guiColorsMenu;
    bool isColorMenuOpen = false;

    public List<Material> materialList;
    int curMaterialColor;

    public GameObject selectedColor;
    public GameObject highLightObj;
    public Transform buttonStartPoint;

    private static ColorsMenu _instance;
    public static ColorsMenu Instance {
        get
        {
            return _instance;
        }
    }

    private void Awake()
    {
        if (_instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
        }
       // UnityEngine.XR.XRSettings.eyeTextureResolutionScale = 2.0f;
    }

    void Start ()
    {
        isColorMenuOpen = false;
        guiColorsMenu.SetActive(isColorMenuOpen);
        OnSelectColor(GlobalData.curSelectedColor);
    }
	

	void Update ()
    {
		
	}


    public void OnOpenColorsMenu()
    {
        AudioMgr.Instance.PlaySFX();

        isColorMenuOpen = !isColorMenuOpen;
        guiColorsMenu.SetActive(isColorMenuOpen);
    }


    public void OnSelectColor(int colorId)
    {
        AudioMgr.Instance.PlaySFX();
        GlobalData.curSelectedColor = colorId;
        curMaterialColor = colorId;
        if (curMaterialColor < 0 || curMaterialColor >= materialList.Count)
        {
            curMaterialColor = 0;
        }
        selectedColor.GetComponent<Renderer>().material = materialList[curMaterialColor];
        float xPos = (curMaterialColor % 3) * 0.32f;
        float yPos = (curMaterialColor / 3) * 0.2f;
        highLightObj.transform.localPosition = buttonStartPoint.localPosition + new Vector3(xPos, -yPos, 0);
    }


    public Material GetCurrentColor()
    {
        return materialList[GlobalData.curSelectedColor];
    }

}

