﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class IntExtMenu : MonoBehaviour
{

    public GameObject guiMenu;
    bool isGuiMenuOpen = false;

    [SerializeField]
    private List<GameObject> interiorTypes;

    private bool _isFurniture=false;

    [SerializeField]
    private Toggle toggle;

    private static IntExtMenu _instance;
    public static IntExtMenu Instance
    {
        get
        {
            return _instance;
        }
    }

    private void Awake()
    {
        if (_instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    void Start()
    {
        isGuiMenuOpen = false;
        guiMenu.SetActive(isGuiMenuOpen);
        GlobalData.curScene = SceneManager.GetActiveScene().buildIndex;
        InitToggleBtn();
    }

    public void OnOpenMenu()
    {
        AudioMgr.Instance.PlaySFX();

        isGuiMenuOpen = !isGuiMenuOpen;
        guiMenu.SetActive(isGuiMenuOpen);
    }


    public void OnInteriorOrExteriorLoad(int val)
    {
        AudioMgr.Instance.PlaySFX();

        guiMenu.SetActive(false);
        if (GlobalData.curScene != val)
        {
            GlobalData.next_scene_index = val;
            SceneManager.LoadScene(0);
        }
    }

    public void OnFurnitureToggle()
    {
        if (_isFurniture)
        {
            _isFurniture = false;
            GlobalData.isFurniture = false;
             interiorTypes[0].SetActive(true);
            interiorTypes[1].SetActive(false);
        }
        else
        {
            _isFurniture = true;
            GlobalData.isFurniture = true;
            interiorTypes[0].SetActive(false);
            interiorTypes[1].SetActive(true);
        }
    }

    public void InitToggleBtn()
    {
        toggle.isOn = GlobalData.isFurniture;
        _isFurniture = GlobalData.isFurniture;
        if (_isFurniture)
        {
            interiorTypes[0].SetActive(false);
            interiorTypes[1].SetActive(true);
        }
        else
        {
            interiorTypes[0].SetActive(true);
            interiorTypes[1].SetActive(false);
        }
    }
}
