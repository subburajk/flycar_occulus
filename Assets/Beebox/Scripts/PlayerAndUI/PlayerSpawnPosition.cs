﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerSpawnPosition : MonoBehaviour, IPointerClickHandler
{
    public bool isUpdateOnAwake = false;
    public Player player;
    public bool isPlayerWantToRotate = false;
    public GameObject CentreObj;
    public bool isPaint;
    public bool isPaintable;

    private void Start()
    {
        if (Player.instance!=null)
        {
            player = Player.instance;
        }
        if (player != null && isUpdateOnAwake)
        {
            UpdatePlayerPosition();
        }
        if (isPaint)
        {
            player.ShowOrHideMenu(isPaintable);
        }
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            UpdatePlayerPosition();
        }
    }

    public void UpdatePlayerPosition()
    {
        Debug.Log("Position");
        player.UpdatePosition(this.transform);
        if (isPlayerWantToRotate)
        {
            player.SetPlayerRotation(isPlayerWantToRotate, CentreObj);
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        AudioMgr.Instance.PlaySFX();
        UpdatePlayerPosition();
    }
}
