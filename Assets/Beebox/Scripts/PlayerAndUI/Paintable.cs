﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


[RequireComponent(typeof(MeshCollider))]
public class Paintable : MonoBehaviour, IPointerClickHandler 
{
    public void OnPointerClick(PointerEventData eventData)
    {
        AudioMgr.Instance.PlaySFXPaint();
        this.GetComponent<Renderer>().material = ColorsMenu.Instance.GetCurrentColor();
    }
}
