﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class UIInteraction3D :MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    public bool isHidePanel=false;
    public float delayTime = 0;

    [SerializeField]
    private GameObject higlightObject;

    public UnityEvent ClickEvent;
    public UnityEvent delayEvent;

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.N))
        {
            StartCoroutine(StartPointerClick());
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        StartCoroutine(StartPointerClick());
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (higlightObject != null)
        {
            higlightObject.SetActive(true);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (higlightObject != null)
        {
            higlightObject.SetActive(false);
        }
    }

    public IEnumerator StartPointerClick()
    {
        ClickEvent.Invoke();
        yield return new WaitForSeconds(delayTime);
        delayEvent.Invoke();
        yield return new WaitForSeconds(0.1f);
        if (isHidePanel)
        {
            this.transform.parent.gameObject.SetActive(false);
        }

    }
}
