﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public static Player instance;
    private Vector2 touchPadPos;

    [SerializeField]
    private bool isRotateAround;
    Transform centerObject;
    [SerializeField]
    private float rotatioSpeed = 5;

    [Header("Player Role")]
    [SerializeField]
    private GameObject mainMenu;
    [SerializeField]
    private GameObject colorMenu;

    [SerializeField]
    private bool isDestroyable;

    private bool mine;

    public GameObject lastStandObj;

    private void Awake()
    {
        if (!isDestroyable) {
            if (instance == null)
            {
                instance = this;
            }
            else if (instance != this)
            {
                Destroy(this.gameObject);
            }
            DontDestroyOnLoad(this);
        }
        else
        {
            if (instance != null)
            {
                Destroy(this.gameObject);
            }
            else
            {
                instance = this;
            }
        }
        
    }

    public Camera centerEyeCamera;

    public void UpdatePosition(Transform _transform)
    {
        Vector3 pos = _transform.position;
        pos.y = transform.position.y;
        transform.position =pos;
        transform.rotation = _transform.rotation;
        if (lastStandObj!=null) {
            lastStandObj.gameObject.SetActive(true);
        }
        lastStandObj = _transform.gameObject;
        lastStandObj.SetActive(false);
        if (DebugLogger.instance!=null) {
            DebugLogger.instance.UpdateDebugCamera(centerEyeCamera, this.transform);
        }
    }

    private void Update()
    {
        if (isRotateAround && centerObject != null)
        {
            touchPadPos = OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad);
            this.transform.RotateAround(centerObject.position, new Vector3(0, -1.0f, 0), touchPadPos.x * Time.deltaTime * rotatioSpeed);
        }
    }

    public void SetRotateAroudObject(Transform _centerObj)
    {
        centerObject = _centerObj;
    }

    public void SetPlayerRotation(bool value,GameObject obj)
    {
        isRotateAround = value;
        centerObject = obj.transform;
    }

    public void ShowOrHideMenu(bool isPaint)
    {
        if (mainMenu!=null && colorMenu!=null)
        {
            mainMenu.SetActive(isPaint);
            colorMenu.SetActive(isPaint);
        }
    }
}