﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class IUIScrollInteract : MonoBehaviour {
    public abstract void OnClicked();
    public abstract void OnEnter(); // hover enter
    public abstract void OnExit(); // hover exit
}
