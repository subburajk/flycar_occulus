﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;

[RequireComponent(typeof(Image))]
public class UIButtton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler
{
    [SerializeField]
    private bool isToggle=false;
    [SerializeField]
    private bool isToggleActive = false;
    private int toggleIndex=0;

    [SerializeField]
    private Image targetGraphics;

    public UnityEvent OnClick;

    public SpriteState[] spriteState;

    private void Start()
    {
        if (isToggleActive)
        {
            toggleIndex = 1;
            targetGraphics.sprite = spriteState[toggleIndex].disabledSprite;
        }
    }
    
    public void OnPointerClick(PointerEventData eventData)
    {
        OnClick.Invoke();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        targetGraphics.sprite = spriteState[toggleIndex].highlightedSprite;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        targetGraphics.sprite = spriteState[toggleIndex].disabledSprite;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        targetGraphics.sprite = spriteState[toggleIndex].pressedSprite;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (isToggle)
        {
            toggleIndex = toggleIndex == 0 ? 1 : 0;
        }
        targetGraphics.sprite = spriteState[toggleIndex].highlightedSprite;
    }

    public void OnChangeToggle(int index)
    {
        if (isToggle)
        {
            toggleIndex = index <=1 ? index : 0;
        }
        targetGraphics.sprite = spriteState[toggleIndex].disabledSprite;
    }
}
