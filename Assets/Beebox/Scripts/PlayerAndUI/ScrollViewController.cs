﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollViewController : IUIScrollInteract {

    public ColorBlock viewColor;
    public Scrollbar scrollBar;
    private ColorBlock cb;
    LaserPointer lp;
    public LaserPointer.LaserBeamBehavior laserBeamBehavior;

    // Use this for initialization
    void Start()
    {
       lp = FindObjectOfType<LaserPointer>();
        if (!lp)
        {
            Debug.LogError("Debug UI requires use of a LaserPointer and will not function without it. Add one to your scene, or assign the UIHelpers prefab to the DebugUIBuilder in the inspector.");
            return;
        }
        lp.laserBeamBehavior = LaserPointer.LaserBeamBehavior.On;
        
        StartCoroutine(Initialize());
    }

    IEnumerator Initialize()
    {
        cb = scrollBar.colors;
        yield return null;
    }

    public override void OnClicked()
    {
        Debug.Log("Clicked");
    }

    public override void OnEnter()
    {
        cb.normalColor = viewColor.highlightedColor;
        scrollBar.colors = cb;
       //scrollBar.GetComponent<>
    }

    public override void OnExit()
    {
        cb.normalColor = viewColor.normalColor;
        scrollBar.colors = cb;
    }
    
    
}
