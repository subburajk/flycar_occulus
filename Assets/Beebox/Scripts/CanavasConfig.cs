﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanavasConfig : MonoBehaviour {

    LaserPointer lp;
    public LaserPointer.LaserBeamBehavior laserBeamBehavior;

    private void Start()
    {
        if (this.GetComponent<Canvas>()!=null) {
            this.GetComponent<Canvas>().worldCamera = Player.instance.centerEyeCamera;
        }
        lp = FindObjectOfType<LaserPointer>();
        if (!lp)
        {
            Debug.LogError("Debug UI requires use of a LaserPointer and will not function without it. Add one to your scene, or assign the UIHelpers prefab to the DebugUIBuilder in the inspector.");
            return;
        }
        lp.laserBeamBehavior = LaserPointer.LaserBeamBehavior.On;
    }
}
