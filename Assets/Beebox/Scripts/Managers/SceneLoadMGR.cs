﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoadMGR : MonoBehaviour {

    public static SceneLoadMGR Instance;
    public int currentSceneNo;

    public string[] sceneNames;

    public void Awake()
    {
        if (Instance==null)
        {
            Instance = this;
        }
        else
        {
            DestroyObject(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    public void LoadScene(int sceneNo)
    {
        currentSceneNo = sceneNo;
        StartCoroutine(LoadAsynScene(sceneNo));
    }

    IEnumerator LoadAsynScene(int sceneNo)
    {
        yield return null;
        SceneManager.LoadSceneAsync(sceneNames[sceneNo],LoadSceneMode.Additive);
    }

    public void UnLoadScene(int sceneNo)
    {
        StartCoroutine(UnLoadAsynScene(sceneNo));
    }

    IEnumerator UnLoadAsynScene(int sceneNo)
    {
        yield return null;
        SceneManager.UnloadSceneAsync(sceneNames[sceneNo]);
    }

    public void LoadRoomScene()
    {
        LoadAsynScene(0);
        UnLoadAsynScene(currentSceneNo);
    }
}
