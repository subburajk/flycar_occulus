﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalData : MonoBehaviour {

    public static int curScene = 0;
    public static int curSelectedColor = 0;

    public static int next_scene_index = 1;

    public static bool isFurniture = false;

}
