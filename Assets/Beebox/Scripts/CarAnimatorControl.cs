﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarAnimatorControl : MonoBehaviour
{

    int curId = 0;
    public int endIndex;
    public Animator animator;

    public List<Texture> customeTextures;
    public int endCustTextureId;
    public int curCustTextureId;
    public Material custMaterial;

    private void Start()
    {
        endCustTextureId = customeTextures.Count;
        custMaterial.mainTexture = customeTextures[0];
    }

    public void OnAnimBtnClicked()
    {
        Debug.Log("hg");
        //animator = GetComponentInChildren<Animator>();
        if (animator != null)
        {

            curId++;
            if (curId == endIndex)
            {
                curId = 0;
            }
            animator.SetInteger("AnimOrder", curId);
            animator.SetTrigger("Next");
        }
    }

    public void OnColorChange()
    {
        

            curCustTextureId++;
            if (curCustTextureId == endCustTextureId)
            {
                curCustTextureId = 0;
            }
            custMaterial.mainTexture = customeTextures[curCustTextureId];

        
    }
}
