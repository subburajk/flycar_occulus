﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugLoggerLocal : MonoBehaviour {

    public static DebugLoggerLocal instance;
    public Text debugText;

    // Use this for initialization
    void Awake()
    {
        
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    public void Show()
    {
        this.gameObject.SetActive(true);
    }

    public void Hide()
    {
        this.gameObject.SetActive(true);
    }

    public void Log(string val)
    {
        StartCoroutine(UpdateLog(val));
    }

    IEnumerator UpdateLog(string val)
    {
        debugText.text += val;
        yield return null;
    }

    public void UpdateDebugCamera(Camera camera,Transform player)
    {
        var _pos = player.position;
        _pos.z = player.position.z + 3.20f;
        this.transform.position = _pos;
        this.GetComponent<Canvas>().worldCamera = camera;
    }

    public void Clear()
    {
        debugText.text = "";
    }
}
